(ns silurus.store.datalevin
  (:require [silurus.store :as st]
            [silurus.specs :as sp]
            #?@(:bb  [[babashka.pods :as pods]]
                :clj [[datalevin.core :as d]])
            [medley.core :as med]
            [taoensso.timbre :as log]
            [clojure.core.async :as as]))

#?(:bb (do (pods/load-pod "dtlv")
           (require '[pod.huahaiy.datalevin :as d]))
   :clj nil)

(def schema {:node       {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/keyword}
             :url        {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/string}
             :params     {:db/cardinality :db.cardinality/one}
             :status     {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/keyword}
             :export     {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/boolean}
             :entity     {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/keyword}
             :parent     {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/ref}
             :ident      {:db/unique    :db.unique/identity
                          :db/valueType :db.type/string}
             :data       {:db/cardinality :db.cardinality/one}
             :priority   {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/long}
             :depth      {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/long}
             :score      {:db/cardinality :db.cardinality/one
                          :db/valueType   :db.type/double}})

(defn checkout-next-action! [conn env]
  (let [results (d/q '[:find ?n ?ident ?depth ?score
                       :where
                       [?n :node :act]
                       [?n :status :pending]
                       [?n :ident ?ident]
                       [?n :depth ?depth]
                       [(get-else $ ?n :score 0) ?score]]
                     (d/db conn))]
    (when (seq results)
      (let [sort-type (or (-> env :config :strategy) :breadth-first)
            [node ident _ _] (first (sort-by #(get % 3) results))
            entity (d/entity (d/db conn) [:ident ident])]
        ;; Avoid returning the stateful datalevin entity
        (into {} entity)))))

(defn node-exists? [conn _ ident]
  (d/entid (d/db conn) [:ident ident]))

(defn val-to-link [x]
  (cond
    (map? x) [:ident (:ident x)]
    (nil? x) nil
    :else [:ident x]))

(defn node-by-ident [conn env node-type ident]
  (let [db (d/db conn)]
    (d/entity db [:ident ident])))

(defn commit-nodes!
  [conn env exprs]
  (let [all-nodes (for [expr exprs
                        :let [[mode {:keys [ident] :as n}] expr
                              node-type (sp/node-type n)
                              old-node (node-by-ident conn env node-type ident)
                              node (into {}
                                         (case mode
                                           :create (if old-node nil n)
                                           :update (med/deep-merge (dissoc (into {} old-node) :parent) (dissoc n :parent))
                                           :replace n))]]
                    (-> node
                        (med/assoc-some :parent (val-to-link (:parent-ident node)))
                        (assoc :node node-type)
                        (dissoc :act-children-idents :data-children-idents)))
        node-types (group-by :node all-nodes)]
    (log/infof "Adding %d action nodes and %d data nodes." (count (:act node-types)) (count (:data node-types)))
    (let [tx (d/transact! conn all-nodes)]
      conn)))

(defn retry-action-nodes! [conn _ idents]
  (let [trxs (for [ident idents
                   :let [entity (d/entity (d/db conn) [:ident ident])]
                   :when (= (:status entity) :failure)]
               (assoc entity :status :pending))]
      (d/transact! conn trxs)))

(defn db-id->ident [conn {:db/keys [id]}]
  (->> id
       (d/entity (d/db conn))
       (:ident)))

(defn data-node-convert-to-idents [conn ks e]
  (reduce
   (fn [acc k]
     (update acc k (partial db-id->ident conn)))
   e ks))

(defn data-nodes-list
  ([conn env entity]
   (let [ids (if entity
               (d/q '[:find [?ident ...]
                      :in $ ?entity
                      :where
                      [?n :node :data]
                      [?n :entity ?entity]
                      [?n :ident ?ident]]
                    (d/db conn) entity)
               (d/q '[:find [?ident ...]
                      :where
                      [?n :node :data]
                      [?n :ident ?ident]]
                    (d/db conn)))]
     (map #(as-> [:ident %] $
             (d/entity (d/db conn) $)
             (d/touch $)
             (into {} $)
             ((partial data-node-convert-to-idents conn [:source :data-parent]) $)
             (dissoc $ :db/id))
          ids)))
  ([conn env] (data-nodes-list conn env nil)))

(defn error-nodes-list [conn _]
  (let [ids (d/q '[:find [?ident ...]
                   :where
                   [?n :status :failure]
                   [?n :ident ?ident]
                   [?n :node :act]]
                 (d/db conn))]
    (->> ids
         (map #(as-> [:ident %] $
                 (d/entity (d/db conn) $)
                 (d/touch $)
                 (into {} $)
                 ((partial data-node-convert-to-idents conn [:parent]) $)
                 (dissoc $ :db/id))))))

(defn parent [store env node]
  (:parent node))

(defn act-children [store env node]
  (let [children (:_parent node)]
    (filter #(= :act (:node %)) children)))

(defn data-children [store env node]
  (let [children (:_parent node)]
    (filter #(= :data (:node %)) children)))

(defn data-ancestors [store env sm]
  (when-let [par (parent store env sm)]
    (concat (data-children store env par)
            (lazy-seq (data-ancestors store env par)))))

(defn act-ancestors [store env sm]
  (when-let [par (parent store env sm)]
    (conj par (lazy-seq (act-ancestors store env par)))))

(defn make-store [{:keys [store] :as config}]
  (let [uri (:uri store)
        conn (d/get-conn uri schema)]
    #?(:bb (with-meta conn
             {`st/nodes-by-ident (fn [conn env idents] (nodes-by-ident conn env idents))
              `st/node-exists? (fn [conn env ident] (node-exists? conn env ident))
              `st/checkout-next-action! (fn [this env] (checkout-next-action! this env))
              `st/commit-nodes! (fn [conn env exprs] (commit-nodes! conn env exprs))
              `st/retry-action-nodes! (fn [conn env idents] (retry-action-nodes! conn env idents))
              `st/data-nodes-list (fn [this env entity] (data-nodes-list this env entity))
              `st/error-nodes-list (fn [this env] (error-nodes-list this env))
              `st/parent (fn [this env entity] (parent this env entity))
              `st/act-children (fn [this env entity] (act-children this env entity))
              `st/data-children (fn [this env entity] (data-children this env entity))
              `st/data-ancestors (fn [this env entity] (data-ancestors this env entity))
              `st/act-ancestors (fn [this env entity] (act-ancestors this env entity))})
       :clj conn)))

(extend-protocol st/PSilurusStore
  clojure.lang.Atom
  (st/node-by-ident [conn env node-type ident] (node-by-ident conn env node-type ident))
  (st/node-exists? [conn env ident] (node-exists? conn env ident))
  (st/checkout-next-action! [this env] (checkout-next-action! this env))
  (st/commit-nodes! [conn env exprs] (commit-nodes! conn env exprs))
  (st/retry-action-nodes! [conn env idents] (retry-action-nodes! conn env idents))
  (st/data-nodes-list [this env entity] (data-nodes-list this env entity))
  (st/error-nodes-list [this env] (error-nodes-list this env))
  (st/parent [this env entity] (parent this env entity))
  (st/act-children [this env entity] (act-children this env entity))
  (st/data-children [this env entity] (data-children this env entity))
  (st/data-ancestors [this env entity] (data-ancestors this env entity))
  (st/act-ancestors [this env entity] (act-ancestors this env entity)))
