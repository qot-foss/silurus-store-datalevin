(ns silurus.store.datalevin-test
  (:require [clojure.test :refer :all]
            [silurus.silurus-store-datalevin :refer :all]
            [expectations.clojure.test ::refer [defexpect expect]]))


(defexpect connection-creation
  (let [conn (make-store {:store {:uri "/tmp/silurus-datalevin-test"}})]
    conn))
