(ns silurus.store.datalevin-test
  (:require [clojure.test :refer :all]
            [silurus.store.datalevin :refer :all]
            [expectations.clojure.test :refer [defexpect expect in expecting more-of more]]
            [datalevin.core :as d]))

(def ^:dynamic  test-db-uri nil)

(defn database-reset-fixture [test-fn]
  (let [uri (str "/tmp/silurus-datalevin-tests/" (rand-int 10000000))
        conn (d/create-conn uri)]
    (binding [test-db-uri uri]
      (test-fn)
      (d/clear conn)
      (d/close conn))))

(use-fixtures :each database-reset-fixture)

(defexpect connection-creation
  (let [conn (make-store {:store {:uri test-db-uri}})]
    (expect #(instance? clojure.lang.Atom %) conn)))

(defexpect read-write-nodes
  (let [conn (make-store {:store {:uri test-db-uri}})]
    (expecting "adding seed"
               (expect vector? (commit-nodes! conn {:store conn} nil [{:url "https://example.com/1" :analyzer ::test}
                                                                      {:url "https://example.com/2" :analyzer ::test}]))
               (expect {:ident "https://example.com/1" :analyzer ::test :node :act :status :pending}
                       (in (d/entity (d/db conn) [:ident "https://example.com/1"]))))
    (expecting "next act node"
               (expect (more-of {:keys [node depth status]}
                                :act node
                                0 depth
                                :pending status)
                       (checkout-next-action! conn {:store conn :config {:strategy :depth-first}})))
    (expecting "adding children"
               (expecting "writing full results"
                          (expect vector? (commit-nodes! conn {:store conn}
                                                         {:url "https://example.com/1" :status :success
                                                          :ident "https://example.com/1" :depth 0}
                                                         [{:url "https://example.com/3" :analyzer ::test}
                                                          {:data {:id 1} :ident "b1" :entity :brand}])))
               (expecting "check parent"
                          (expect {:ident "https://example.com/1" :analyzer ::test :node :act :status :success}
                                  (in (d/entity (d/db conn) [:ident "https://example.com/1"]))))
               (expecting "check child"
                          (expect (more-of {:keys [ident analyzer node status parent depth]}
                                           "https://example.com/3" ident
                                           1 depth
                                           ::test analyzer
                                           :act node
                                           :pending status
                                           #(= "https://example.com/1" (:ident (first %))) parent)
                                  (d/entity (d/db conn) [:ident "https://example.com/3"])))
               (expecting "check data"
                          (expect (more-of {:keys [ident data entity node]
                                            {source-ident :ident} :source}
                                           "b1" ident
                                           {:id 1} data
                                           :data node
                                           :brand entity
                                           "https://example.com/1" source-ident)
                                  (d/entity (d/db conn) [:ident "b1"])))
               (expecting "next deeper act node"
                          (expect (more-of {:keys [node depth status ident]}
                                           :act node
                                           1 depth
                                           :pending status
                                           "https://example.com/3" ident)
                                  (checkout-next-action! conn {:store conn :config {:strategy :depth-first}})))
               (expecting "data parent"
                          (expect vector? (commit-nodes! conn {:store conn}
                                                         {:url "https://example.com/3" :status :success :ident "https://example.com/3"}
                                                         [{:data {:id 1} :ident "i1" :entity :item :data-parent "b1"}]))
                          (expect (more-of {{data-parent-ident :ident} :data-parent}
                                           data-parent-ident "b1")
                                  (d/entity (d/db conn) [:ident "i1"])))
               (expecting "last act node"
                          (expect (more-of {:keys [node depth status ident]}
                                           :act node
                                           0 depth
                                           :pending status
                                           "https://example.com/2" ident)
                                  (checkout-next-action! conn {:store conn :config {:strategy :depth-first}}))))
    (expecting "reading nodes"
               (expecting "by ident"
                          (expect (more-of [{:keys [node ident]}]
                                           :act node
                                           "https://example.com/3" ident)
                                  (nodes-by-ident conn {:store conn} ["https://example.com/3"])))
               (expecting "list data nodes by entity"
                          (expect (more-of [{:keys [entity node]} :as l]
                                           :brand entity
                                           :data node
                                           #(= 1 (count %)) l)
                                  (data-nodes-list conn {:store conn} :brand)))
               (expecting "list data all nodes"
                          (expect (more #(= 2 (count %)) #(= #{:brand :item} (into #{} (map :entity %))))
                                  (data-nodes-list conn {:store conn} nil))))))

(defexpect retry
  (let [conn (make-store {:store {:uri test-db-uri}})]
    (expect vector? (commit-nodes! conn {:store conn} nil [{:url "https://example.com/1" :analyzer ::test}
                                                           {:url "https://example.com/2" :analyzer ::test}]))
    (expecting "checking update"
               (expect vector? (commit-nodes! conn {:store conn} nil [{:ident "https://example.com/1" :status :failure}] :update-actions true))
               (expect {:status :failure} (in (d/entity (d/db conn) [:ident "https://example.com/1"]))))
    (expecting "listing errors"
               (expect (more #(= 1 (count %))
                             #(= "https://example.com/1" (-> % (first) (:ident)))
                             #(= :failure (-> % (first) (:status))))
                       (error-nodes-list conn {:store conn})))
    (expecting "commiting retry"
               (expect vector? (retry-action-nodes! conn {:store conn} ["https://example.com/1"]))
               (expect {:status :pending} (in (d/entity (d/db conn) [:ident "https://example.com/1"]))))
    (expecting "no errors left"
               (expect empty?
                       (error-nodes-list conn {:store conn})))))
